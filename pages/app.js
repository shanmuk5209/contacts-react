import React, { Component } from 'react';
// import '../App.css';
import Contact from '../components/contact'
import Search from '../components/Search'
import EditModal from '../components/EditModal'
import CreateModal from '../components/CreateModal'
import "../styles/app.module.css"
const contactsData = require('../contacts.json');
if (typeof window !== 'undefined'){
  localStorage.setItem("data", JSON.stringify(contactsData));
}



class App extends Component {
  constructor(props) {
    super(props);
    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.createShow = this.createShow.bind(this);
    this.createClose = this.createClose.bind(this);
    this.state = {
      show: false,
      create:false,
      contacts: [],
      searchItem: "",
      editIddata: {},
    }
  }
  componentDidMount(){
    if (typeof window !== 'undefined'){
    this.setState({contacts:JSON.parse(localStorage.getItem("data"))})
    }
  }
  SearchHandler = (element) => {
    this.setState({ searchItem: element.target.value })
  }
  DeleteHandler = (element) => {
    // eslint-disable-next-line eqeqeq
    const filteredData = [...this.state.contacts].filter(e => e.id != element.target.id);
    this.setState(() => {
      return { contacts: filteredData }
    })
  }
  EditHandler = (id) => {
    const editData = Object.values(this.state.contacts).filter(e => e.id === parseInt(id.target.id));
    this.setState({ editIddata: editData })
    this.setState({ show: true });
  }
  UpdateHandler = (e) => {
    e.preventDefault();
    const array = [...this.state.contacts]
    let updatedData = array.map(contact => {
      if (contact.id === parseInt(e.target.id)) {
        contact.first_name = e.target.inputFirstName.value;
        contact.last_name = e.target.inputLastName.value;
        contact.email = e.target.inputEmailAddress.value;
        contact.phone = e.target.inputPhoneNumber.value;
      }
      return contact
    })
    this.setState(() => {
      return { contacts: updatedData }
    });
    this.setState({ show: false });
  }
  CreateHandler = (e) => {
    e.preventDefault();

    const array = [...this.state.contacts];
    const NewData = {
      id: array[array.length - 1].id+1,
      first_name: e.target.inputFirstName.value,
      last_name: e.target.inputLastName.value,
      email: e.target.inputEmailAddress.value,
      avatar_url: `https://robohash.org/${e.target.inputFirstName.value}?size=100x100&set=set1`,
      phone: e.target.inputPhoneNumber.value
    }
    array.push(NewData);
    this.setState(() => {
      return { contacts: array }
    })
    this.setState({ create: false });
  }
  handleClose() {
    this.setState({ show: false });
  }

  handleShow() {
    this.setState({ show: true });
  }
  createClose() {
    this.setState({ create: false });
  }

  createShow() {
    this.setState({ create: true });
  }
  render() {
    return (
      <div className="container">
        <div className="row">
          <Search emit={this.edittoggle} handleShow={this.createShow} fun={this.SearchHandler}></Search>
          <table className="table table-hover table-dark table-borderless">
            <thead>
              <tr>
                <th>S.No</th>
                <th>Profile Picture</th>
                <th>Full Name</th>
                <th>Phone Number</th>
                <th>Operations</th>
              </tr>
            </thead>
            <tbody>
              {Object.values(this.state.contacts).filter(e => e.first_name.toLowerCase().includes(this.state.searchItem.toLowerCase()) || e.last_name.toLowerCase().includes(this.state.searchItem.toLowerCase())).map(contact =>
                < Contact key={contact.id} handleShow={this.handleShow} EditFun={this.EditHandler} DeleteFun={this.DeleteHandler} data={contact}></Contact>
              )}
            </tbody>
          </table>
        </div>
        {
          this.state.show && <EditModal handleClose={this.handleClose} submit={this.UpdateHandler} show={this.state.show} data={this.state.editIddata}></EditModal>
        }
        {
        this.state.create&&<CreateModal show={this.state.create} handleClose={this.createClose} submit={this.CreateHandler}></CreateModal>
      }
      </div>
    );
  }

}
export default App;